"use client"
import { useState } from "react"

export default function Calculator() {
  const math = require("mathjs")
  const [res, setRes] = useState("")

  function handleChange(event) {
    if (res === "ERROR" && event.target.value != "=") {
      setRes(0)
    }
    if (event.target.value === "C" && res !="ERROR") {
      setRes("")
    }
    if (event.target.value === "=" && (res == "" || res =="ERROR")) {
      setRes("")
    }
    if(event.target.value ==="=" && res !== "" && res !=="ERROR"){
      try {
        setRes(math.evaluate(res))
      } catch (error) {
        setRes("ERROR")
      }

    }
    else if(event.target.value != "=" && event.target.value != "C") {
      setRes(res + event.target.value)
    }
  }

  return <>
    <header className="text-center">
      <br />
      <h1 className="text-5xl font-medium">
        Calculatrice
      </h1>
      <h1 className="text-xl">La calculatrice</h1>
    </header>
    <br />
    <section className="flex justify-center items-center poppins">
      <div className="border p-3 lg:p-11 rounded-lg bg-emerald-50 shadow-md">
        <input value={res}
          type="text"
          className=" border border-slate-300 p-3 my-4 bg-emerald-50 rounded-md
   w-80 poppins text-right h-20 shadow-sm focus:outline-none font-bold"
          id="result"
          readOnly />
        <div className="flex items-center justify-between font-bold">
          <button onClick={handleChange} value="C" className=" border shadow-md w-14 h-14 rounded-md my-2 hover:bg-red-300 bg-red-500 w-[9rem]">
            C
          </button>
          <button onClick={handleChange} value="%" className=" border shadow-md w-14 h-14 rounded-md my-2 hover:bg-slate-100 bg-slate-200">
            %
          </button>
          <button onClick={handleChange} value="/" className=" border shadow-md w-14 h-14 rounded-md my-2 hover:bg-slate-100 bg-slate-200">
            /
          </button>
        </div>
        <div className="flex items-center justify-between font-bold">
          <button onClick={handleChange} value="7" className="items-center justify-center border shadow-md w-14 h-14 rounded-md my-2 hover:bg-slate-100 bg-slate-200">
            7
          </button>
          <button onClick={handleChange} value="8" className=" border shadow-md w-14 h-14 rounded-md my-2 hover:bg-slate-100 bg-slate-200">
            8
          </button>
          <button onClick={handleChange} value="9" className=" border shadow-md w-14 h-14 rounded-md my-2 hover:bg-slate-100 bg-slate-200">
            9
          </button>
          <button onClick={handleChange} value="*" className=" border shadow-md w-14 h-14 rounded-md my-2 hover:bg-slate-100 bg-slate-200">
            *
          </button>
        </div>
        <div className="flex items-center justify-between font-bold">
          <button onClick={handleChange} value="4" className="flex items-center justify-center border shadow-md w-14 h-14 rounded-md my-2 text--700 hover:bg-slate-100 bg-slate-200 ">
            4
          </button>
          <button onClick={handleChange} value="5" className=" border shadow-md w-14 h-14 rounded-md my-2 hover:bg-slate-100 bg-slate-200">
            5
          </button>
          <button onClick={handleChange} value="6" className=" border shadow-md w-14 h-14 rounded-md my-2 hover:bg-slate-100 bg-slate-200">
            6
          </button>
          <button onClick={handleChange} value="+" className=" border shadow-md w-14 h-14 rounded-md my-2 hover:bg-slate-100 bg-slate-200">
            +
          </button>
        </div>
        <div className="flex items-center justify-between font-bold">
          <button onClick={handleChange} value="1" className="flex items-center justify-center border shadow-md w-14 h-14 rounded-md my-2  hover:bg-slate-100 bg-slate-200">
            1
          </button>
          <button onClick={handleChange} value="2" className=" border shadow-md w-14 h-14 rounded-md my-2  hover:bg-slate-100 bg-slate-200">
            2
          </button>
          <button onClick={handleChange} value="3" className=" border shadow-md w-14 h-14 rounded-md my-2 hover:bg-slate-100 bg-slate-200">
            3
          </button>
          <button onClick={handleChange} value="-" className=" border shadow-md w-14 h-14 rounded-md my-2 hover:bg-slate-100 bg-slate-200">
            -
          </button>
        </div>
        <div className="flex items-center justify-between font-bold">
          <button onClick={handleChange} value="0" className=" border shadow-md w-14 h-14 rounded-md my-2 hover:bg-slate-100 bg-slate-200">
            0
          </button>
          <button onClick={handleChange} value="." className=" border shadow-md w-14 h-14 rounded-md my-2 hover:bg-slate-100 bg-slate-200">
            .
          </button>

          <button onClick={handleChange} value="=" className="flex items-center justify-center border shadow-md w-14 h-14 rounded-md my-2 text-lime-700 hover:bg-lime-50 bg-lime-200 text-xl w-[9rem]">
            =
          </button>
        </div>
      </div>
    </section>

  </>
}