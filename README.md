## Calculatrice
Une simple calculatrice fait au courant d'une journée dans le cadre d'un processus d'embauche

La calculatrice a été fait avec NextJs avec Tailwindcss.

---
Table des matières:

1.Installation  
2.Utilisation  
3.Fonctionnalités  
4.Sources et références


## Installation
Vous pouvez accéder au site qui est déployé par Vercel ci-dessous:
https://calculatrice-three.vercel.app
Vous pouvez aussi cloner le dépot GitLab via HTTPS:
```bash
git clone https://gitlab.com/Fannod/calculatrice.git
# Ensuite dans votre IDE:
npm install
# Pour lancer le localhost:
npm run dev
```
## Utilisation
Vous avez le choix d'utiliser le lien ci-dessous:  
https://calculatrice-three.vercel.app
ou bien, d'utiliser une version locale avec un dépot locale que vous avez cloné.
```bash
# Pour lancer le localhost:
npm run dev
```
Ouvrez [http://localhost:3000](http://localhost:3000) avec votre navigateur de choix et admirez la belle calculatrice!

## Fonctionnalités

Comme vous pouvez vous en douter, c'est une calculatrice qui effectue des opérations mathématiques standard telles que l'addition, la soustraction, la multiplication, et la division, ainsi que des équations modulaires.

Le fonctionnement est simple, vous appuyez des chiffres alterné avec des opérateurs arithmétiques (+-*/%) et lorsque vous appuyez sur le gros bouton vert (=), vous avez votre résultat. Le gros bouton rouge (C) vous permet d'effacer l'affichage actuel.

En bref, c'est une calculatrice.

## Sources et références
Voici les ressources que j'ai utilisé: 
- [ChatGPT](https://chat.openai.com)
- [Learn Next.js 13](https://www.youtube.com/watch?v=Cq_0nDbIpSk&t=742s)
- [Next.js - The Basics](https://www.youtube.com/watch?v=__mSgDEOyv8&t=2s)
- [Build a simple calculator](https://www.youtube.com/watch?v=Cq_0nDbIpSk&t=742s)
- [Tailwindcss docs](https://v2.tailwindcss.com/docs)
- [Next.js docs](https://nextjs.org/docs)


